package xiao.free.horizontalrefreshlayout.refreshhead;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import xiao.free.horizontalrefreshlayout.R;
import xiao.free.horizontalrefreshlayout.RefreshHeader;

/**
 * Created by xiaoguochang on 2015/12/24.
 */
public class LoadingRefreshHeader implements RefreshHeader {
    private final Context context;
    private ImageView staticLoading;
    private AnimationDrawable animationDrawable;
    public LoadingRefreshHeader(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(ViewGroup container) {
        View view = LayoutInflater.from(context).inflate(R.layout.common_loading_refresh_header, container, false);
        staticLoading = (ImageView) view.findViewById(R.id.static_loading);
        animationDrawable = (AnimationDrawable) staticLoading.getDrawable();
        if (animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
        return view;
    }

    @Override
    public void onStart(int dragPosition, View refreshHead) {
        staticLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDragging(float distance, float percent, View refreshHead) {
//        staticLoading.setRotation(percent * 360);
    }

    @Override
    public void onReadyToRelease(View refreshHead) {
        if (animationDrawable.isRunning()) {
            animationDrawable.stop();
        }
    }

    @Override
    public void onRefreshing(View refreshHead) {
        if (!animationDrawable.isRunning()) {
            animationDrawable.start();
        }
    }
}
