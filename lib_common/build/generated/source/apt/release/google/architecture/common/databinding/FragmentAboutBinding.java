package google.architecture.common.databinding;
import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
public abstract class FragmentAboutBinding extends ViewDataBinding {
    @NonNull
    public final android.widget.Button btn;
    @NonNull
    public final android.widget.TextView txtLocation;
    // variables
    protected java.util.List<google.architecture.common.ui.UserData> mUserList;
    protected google.architecture.common.ui.UserDataOF mUserDataOF;
    protected FragmentAboutBinding(@Nullable android.databinding.DataBindingComponent bindingComponent, @Nullable android.view.View root_, int localFieldCount
        , android.widget.Button btn1
        , android.widget.TextView txtLocation1
    ) {
        super(bindingComponent, root_, localFieldCount);
        this.btn = btn1;
        this.txtLocation = txtLocation1;
    }
    //getters and abstract setters
    public abstract void setUserList(@Nullable java.util.List<google.architecture.common.ui.UserData> UserList);
    @Nullable
    public java.util.List<google.architecture.common.ui.UserData> getUserList() {
        return mUserList;
    }
    public abstract void setUserDataOF(@Nullable google.architecture.common.ui.UserDataOF UserDataOF);
    @Nullable
    public google.architecture.common.ui.UserDataOF getUserDataOF() {
        return mUserDataOF;
    }
    @NonNull
    public static FragmentAboutBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot) {
        return inflate(inflater, root, attachToRoot, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentAboutBinding inflate(@NonNull android.view.LayoutInflater inflater) {
        return inflate(inflater, android.databinding.DataBindingUtil.getDefaultComponent());
    }
    @NonNull
    public static FragmentAboutBinding bind(@NonNull android.view.View view) {
        return null;
    }
    @NonNull
    public static FragmentAboutBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.view.ViewGroup root, boolean attachToRoot, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    @NonNull
    public static FragmentAboutBinding inflate(@NonNull android.view.LayoutInflater inflater, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
    @NonNull
    public static FragmentAboutBinding bind(@NonNull android.view.View view, @Nullable android.databinding.DataBindingComponent bindingComponent) {
        return null;
    }
}