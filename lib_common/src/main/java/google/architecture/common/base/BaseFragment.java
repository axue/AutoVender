package google.architecture.common.base;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;

import google.architecture.common.util.Utils;

public abstract class BaseFragment extends Fragment {

    protected BaseActivity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mActivity = (BaseActivity) context;
    }


    /**
     * 获取宿主Activity
     *
     * @return BaseActivity
     */
    protected BaseActivity getHoldingActivity() {
        return mActivity;
    }



}
