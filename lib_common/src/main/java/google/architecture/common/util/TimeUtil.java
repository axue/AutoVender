package google.architecture.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {

    public static String getCurrentTime(String pattern){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);// HH:mm:ss
        Date date = new Date(System.currentTimeMillis());
        return simpleDateFormat.format(date);
    }
}
