package com.yunqi.usbprinter;

public class Constant {
	public static String TITLE_CN = "安徽安睿机电科技有限公司\n";
	public static String WELCOME_CN = "******欢迎光临******\n";
	public static String STRDATA_CN = "交易编号：%s\n"+"机号：%s\n"+"日期：%s\n"
			+"--------------------------\n"
			+"商品名称       数量  金额\n"
			+"测试名称        1    1.00\n"
			+"测试名称        2    2.00\n"
			+"测试名称        30   3.00\n"
			+"合计                6.00\n"
			+"--------------------------\n"
			+"       实收(人民币): 6.00\n"
			+"售后电话：13888888888\n"
			+"如遇到落药失败并退款失败，请凭此小票到门店退款或者取药";
	public static String STRDATA_US = "There are 10 people waiting in front of you, please note the number\n\n"
										+ "Welcome! We will serve you wholeheartedly.\n";

	/*
	 *  0 打印机正常 、1 打印机未连接或未上电、2 打印机和调用库不匹配 
	 *  3 打印头打开 、4 切刀未复位 、5 打印头过热 、6 黑标错误 、7 纸尽 、8 纸将尽
	 */
	static String Receive_CN = "接收[十六进制]：";
	static String Receive_US = "Receive[Hex]:";
	static String State_CN = "\r\n状态：";
	static String State_US = "\r\nState:";
	static String Normal_CN = "  正常;";
	static String Normal_US = "  Normal;";
	static String NoConnectedOrNoOnPower_CN = "  打印机未连接或未上电;";
	static String NoConnectedOrNoOnPower_US = "  Printer is not connected or not on power;";
	static String PrinterAndLibraryNotMatch_CN = "  打印机和调用库不匹配;";
	static String PrinterAndLibraryNotMatch_US = "  Printer and library does not match;";
	static String PrintHeadOpen_CN = "  打印头打开;";
	static String PrintHeadOpen_US = "  Print head open;";
	static String CutterNotReset_CN = "  切刀未复位;";
	static String CutterNotReset_US = "  Cutter not reset;";
	static String PrintHeadOverheated_CN = "  打印头过热;";
	static String PrintHeadOverheated_US = "  Print head overheated;";
	static String BlackMarkError_CN = "  黑标错误;";
	static String BlackMarkError_US = "  Black mark error;";
	static String PaperExhausted_CN = "  纸尽;";
	static String PaperExhausted_US = "  PaperExhausted;";
	static String PaperWillExhausted_CN = "  纸将尽;";
	static String PaperWillExhausted_US = "  Paper will exhausted;";
	static String Abnormal_CN = "  异常;";
	static String Abnormal_US = "  Abnormal;";
}
