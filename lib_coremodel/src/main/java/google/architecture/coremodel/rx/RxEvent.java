package google.architecture.coremodel.rx;

/**
 * Created by ghcui on 2017/9/8.
 */

public class RxEvent {
    public int code;

    public Object obj;

    public RxEvent() {
    }

    public RxEvent(int code) {
        this.code = code;
    }

    public RxEvent(int code, Object obj) {
        this.code = code;
        this.obj = obj;
    }
}
