package google.architecture.coremodel.exception;

public class LackOfStockException extends Exception {
    public LackOfStockException(String msg){
        super(msg);
    }
}
