package google.architecture.coremodel.navigator;

import android.graphics.Bitmap;


public interface PaymentNavigator extends BaseNavigator {
    void showImage(Bitmap bitmap);
    void onClearAllData(boolean isFinish);
}
