package google.architecture.coremodel.navigator;

import java.util.List;

import google.architecture.coremodel.datamodel.http.entities.ProductData;

public interface StackNavigator extends BaseNavigator {
    void showContent(List<ProductData.Product> list, int page);
    void onOffLineSuccess(int position);
}
