package google.architecture.coremodel.navigator;

import java.util.List;

import google.architecture.coremodel.datamodel.http.entities.ClassifyData;
import google.architecture.coremodel.datamodel.http.entities.ProductData;
import google.architecture.coremodel.datamodel.http.entities.TerInfoData;

public interface ProductNavigator extends PayAndShopCarNavigator{
    void showClassify(ClassifyData classifyData);
    void showContent(List<ProductData.Product> list,int page);
    void onReturnTerInfo(TerInfoData.Data data);
    void onUnlocked(int todo);
    void showUnlockLoading();
    void hideUnlockLoading();
    void onTradeRefundFailed();//退款失败
}
