package google.architecture.coremodel.navigator;

public interface ErrorNavigator {
    void showError(int code, String errorMsg);

}
