package google.architecture.coremodel.navigator;


import google.architecture.coremodel.datamodel.http.entities.ShoppingCarItem;


public interface PayAndShopCarNavigator extends PaymentNavigator {
    void onAddShopCar(ShoppingCarItem item);
}
