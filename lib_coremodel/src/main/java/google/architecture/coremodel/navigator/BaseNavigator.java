package google.architecture.coremodel.navigator;

public interface BaseNavigator extends ErrorNavigator{

    void showLoading();

    void hideLoading();
}
