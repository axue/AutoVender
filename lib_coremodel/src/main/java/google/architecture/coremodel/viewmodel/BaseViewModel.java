package google.architecture.coremodel.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import google.architecture.coremodel.rx.RxBus;
import google.architecture.coremodel.rx.RxEvent;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by dxx on 2017/11/20.
 */

public class BaseViewModel extends AndroidViewModel {

    protected Context mContext;
    protected final CompositeDisposable mDisposable = new CompositeDisposable();
    public BaseViewModel(@NonNull Application application) {
        super(application);
        mContext = application;
    }





    @Override
    protected void onCleared() {
        super.onCleared();
        mDisposable.clear();
    }


}
