package google.architecture.coremodel.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import google.architecture.coremodel.R;
import google.architecture.coremodel.constant.ErrorCode;
import google.architecture.coremodel.datamodel.http.entities.BaseData;
import google.architecture.coremodel.datamodel.http.entities.ProductData;
import google.architecture.coremodel.datamodel.http.repository.AutoVenderDataRepository;
import google.architecture.coremodel.navigator.StackNavigator;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * 保存生命周期和UI所使用的数据
 * Created by dxx on 2017/11/10.
 */

public class StackViewModel extends AndroidViewModel {
    private int mPage = 1;
    public final static int SIZE = 20;
    protected AutoVenderDataRepository mAutoVenderDataRepository;
    protected final CompositeDisposable mDisposable = new CompositeDisposable();
    protected String TAG = this.getClass().getName();
    protected Context mContext;
    /**
     * unit、row、column为0时表示全部
     */
    private int unit = 0;//板号
    private int row = 0;//行号
    private int column = 0;//列号

    public StackViewModel(@NonNull Application application) {
        super(application);
        mContext = application;
        mAutoVenderDataRepository = new AutoVenderDataRepository(mContext);
    }

    public int getPage() {
        return mPage;
    }

    public void setPage(int page) {
        mPage = page;
    }

    private StackNavigator mNavigator;


    public void loadData(String terNo) {
        mAutoVenderDataRepository.getProductListRepository(null, terNo, mPage, SIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProductData>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposable.add(d);
                    }

                    @Override
                    public void onNext(ProductData value) {
                        if (mNavigator != null)
                            mNavigator.showContent(value.getData(), mPage);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError------>" + e.getMessage());
                        if (mNavigator != null)
                            mNavigator.showError(ErrorCode.FAILED_GET_PRODUCT, mContext.getString(R.string.network_error));
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.i(TAG, "onComplete------>");
                    }
                });
    }

    public void searchDrug(String words, String terNo) {
        mAutoVenderDataRepository.searchDrug(words, terNo, mPage, SIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProductData>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposable.add(d);
                    }

                    @Override
                    public void onNext(ProductData value) {
                        if (mNavigator != null)
                            mNavigator.showContent(value.getData(), mPage);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError------>" + e.getMessage());
                        if (mNavigator != null)
                            mNavigator.showError(ErrorCode.FAILED_GET_PRODUCT, mContext.getString(R.string.network_error));
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.i(TAG, "onComplete------>");
                    }
                });
    }


    public void searchDrugByColumn(String terNo,int unit,int row,int column) {
        mAutoVenderDataRepository.searchDrugByColumn(terNo, unit,row,column, mPage, SIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ProductData>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposable.add(d);
                    }

                    @Override
                    public void onNext(ProductData value) {
                        if (mNavigator != null)
                            mNavigator.showContent(value.getData(), mPage);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError------>" + e.getMessage());
                        if (mNavigator != null)
                            mNavigator.showError(ErrorCode.FAILED_GET_PRODUCT, mContext.getString(R.string.network_error));
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.i(TAG, "onComplete------>");
                    }
                });
    }

    public void delDrug(int position, String terNo, String planId) {
        mAutoVenderDataRepository.delDrug(terNo, planId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseData>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mDisposable.add(d);
                    }

                    @Override
                    public void onNext(BaseData value) {
                        if (value.getCode() == 200) {
                            Log.d(TAG, "下架成功！");
                            if (mNavigator != null)
                                mNavigator.onOffLineSuccess(position);
                        } else {
                            mNavigator.showError(ErrorCode.FAILED_OFFLINE, "下架失败！");
                            Log.e(TAG, "错误信息code：" + value.getCode() + ",message:" + value.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError------>" + e.getMessage());
                        mNavigator.showError(ErrorCode.FAILED_OFFLINE, mContext.getString(R.string.network_error));
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.i(TAG, "onComplete------>");
                    }
                });
    }


    public void setNavigator(StackNavigator navigator) {
        mNavigator = navigator;
    }
}
