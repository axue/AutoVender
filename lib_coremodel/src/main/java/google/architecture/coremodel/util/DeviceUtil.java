package google.architecture.coremodel.util;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class DeviceUtil {
    /**
     * 根据wifi信息获取本地mac
     * @param context
     * @return
     */
    public static String getMacAddress(Context context){
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo winfo = wifi.getConnectionInfo();
        String mac =  winfo.getMacAddress();
        return mac;
    }

    public static String getSerialNo(){
        String serialNo = android.os.Build.SERIAL;
        return serialNo;
    }


}
