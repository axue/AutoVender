package google.architecture.coremodel.constant;

public class ErrorCode {
    //服务器异常
    public final static int ERROR_OF_NETWORK = 0x00001;
    //获取终端编号失败
    public final static int FAILED_GET_TERNO = 0x10001;
    //获取分类失败
    public final static int FAILED_GET_CLASSIFY = 0x10002;
    //获取商品列表失败
    public final static int FAILED_GET_PRODUCT = 0x10003;
    //库存不足
    public final static int LACK_OF_STOCK = 0x10004;
    //生成订单失败
    public final static int FAILED_CREATE_ORDER = 0x10005;
    //生成二维码失败
    public final static int FAILED_CREATE_QRCODE = 0x10006;
    //下架失败
    public final static int FAILED_OFFLINE = 0x10007;
    //解锁失败
    public final static int FAILED_UNLOCK = 0x10008;
    //支付成功推送消息
    public final static int CODE_PUSH_PAY_SUCCESS = 10001;
    //商品出柜成功
    public final static String GOODS_OUT_OF_MACHINE_SUCCESS = "AA000001ABCC";
    //商品出柜失败
    public final static String GOODS_OUT_OF_MACHINE_FAILED = "AA000000AACC";
    //温度上报
    public final static String TEMPERATURE_REPORT = "AA000A0EC2DD";
    //硬件通信错误
    public final static String HARDWARE_COMMUNICATEION_ERROR = "AA000001ABEE";
    //光幕偏移错误
    public final static String LIGHT_SCRENN_OFFSET_ERROR = "AA000002ACEE";
    //上位机未收到下位机的指令
    public final static String NO_COMMAND_ERROR = "0000000000";
}
