package google.architecture.coremodel.constant;

public class ARouterPath {
    /**主界面Activity*/
    public static final String MainAct = "/act/main";
    /**产品详情Activity*/
    public static final String ProductDetailAct = "/act/product/detail";
    /**购物车Activity*/
    public static final String ShoppingCarAct = "/act/shpping/car";
    /**搜索Activity*/
    public static final String SearchAct = "/act/search";
    /**商品管理Activity*/
    public static final String SettingsAct = "/act/settings";
}
