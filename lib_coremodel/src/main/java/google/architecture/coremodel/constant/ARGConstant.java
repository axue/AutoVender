package google.architecture.coremodel.constant;

public class ARGConstant {
    public static final String ARG_PRODUCT = "product";
    public static final String ARG_TERNO = "terNo";
    public static final String ARG_PHONE= "phone";
    public static final String ARG_CARGO_INFO = "cargoInfo";
}
