package google.architecture.coremodel.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import google.architecture.coremodel.datamodel.http.ApiConstants;

/**
 * Created by Administrator on 2017/12/12.
 */

public class AppConfigHelper {

    public static final String PREFERENCE_NAME = "AppConfig";
    public static final String DEFAULT_SERIAL_PORT = "9600";
    public static final String DEFAULT_SERIAL_PATH = "/dev/ttyGS3";
    public static final String DEFAULT_SERVER_URL = ApiConstants.Host;
    public static final String DEFAULT_COUNT_DOWN_TIME = "120";//单位为秒
    public static final String DEFAULT_TIME_OUT = "120";//单位为秒
    public static final String DEFAULT_MAX_PRINT_LENGTH = "12";
    private SharedPreferences sharedPreferences;

    public AppConfigHelper(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME,
                Context.MODE_PRIVATE);
    }


    public void saveFirstLauncher(boolean firstLauncher) {
        sharedPreferences.edit().putBoolean("FirstLauncher", firstLauncher).apply();
        sharedPreferences.edit().commit();
    }

    public boolean isFirstLauncher() {
        return sharedPreferences.getBoolean("FirstLauncher", true);
    }
    public void saveServerUrl(String url) {
        sharedPreferences.edit().putString("ServerUrl", url).apply();
        sharedPreferences.edit().commit();
    }

    public String getServerUrl() {
        return sharedPreferences.getString("ServerUrl", DEFAULT_SERVER_URL);
    }

    public void saveSerialPort(String port) {
        sharedPreferences.edit().putString("SerialPort", port).apply();
        sharedPreferences.edit().commit();
    }

    public String getSerialPort() {
        return sharedPreferences.getString("SerialPort", DEFAULT_SERIAL_PORT);
    }

    public void saveSerialPath(String path) {
        sharedPreferences.edit().putString("SerialPath", path).apply();
        sharedPreferences.edit().commit();
    }

    public String getSerialPath() {
        return sharedPreferences.getString("SerialPath", DEFAULT_SERIAL_PATH);
    }

    public void saveHasData(boolean hasData) {
        sharedPreferences.edit().putString("HasData", hasData?"hasData":"noData").apply();
        sharedPreferences.edit().commit();
    }

    public boolean hasData() {
        return TextUtils.equals(sharedPreferences.getString("HasData","noData"),"hasData");
    }

    public void saveCountDownTime(int countDownTime) {
        sharedPreferences.edit().putString("CountDownTime", String.valueOf(countDownTime)).apply();
        sharedPreferences.edit().commit();
    }

    public String getCountDownTime() {
        return sharedPreferences.getString("CountDownTime", DEFAULT_COUNT_DOWN_TIME);
    }

    public void saveTimeOut(int timeOut) {
        sharedPreferences.edit().putString("TimeOut", String.valueOf(timeOut)).apply();
        sharedPreferences.edit().commit();
    }

    public String getTimeOut() {
        return sharedPreferences.getString("TimeOut", DEFAULT_TIME_OUT);
    }

    public void saveMaxPrintLength(int maxPrintLength) {
        sharedPreferences.edit().putString("MaxPrintLength", String.valueOf(maxPrintLength)).apply();
        sharedPreferences.edit().commit();
    }

    public String getMaxPrintLength() {
        return sharedPreferences.getString("MaxPrintLength", DEFAULT_MAX_PRINT_LENGTH);
    }

}
