
package google.architecture.coremodel.callback;



public interface ProductDetailClickCallback {

    void onBuy();
    void onShoppingCar();
    void onJumpShoppingCar();
    void onAdd();
    void onReduce();
    void onBack();
}
