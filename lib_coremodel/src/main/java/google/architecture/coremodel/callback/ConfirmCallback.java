
package google.architecture.coremodel.callback;



public interface ConfirmCallback {
    int ACTION_CANCEL = 0;
    int ACTION_CONFIRM = 1;
    int ACTION_SKIP = 2;
    void onClick(int action);
}
