
package google.architecture.coremodel.callback;


import google.architecture.coremodel.datamodel.http.entities.ProductData;

public interface StackItemClickCallback {
    int ACTION_STACK_MODIFY = 0;//修改库存
    int ACTION_STACK_OFFLINE = 1;//下架

    void onClick(int position,ProductData.Product productItem, int action);
}
