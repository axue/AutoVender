
package google.architecture.coremodel.callback;



public interface LockCallback {
    int ACTION_UNLOCK = 0;
    int ACTION_VERFICATION_UNLOCK = 1;
    void onClick(int action);
}
