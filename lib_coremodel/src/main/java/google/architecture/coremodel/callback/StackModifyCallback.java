
package google.architecture.coremodel.callback;



public interface StackModifyCallback extends ConfirmCallback{
    void onAdd();
    void onReduce();
}
