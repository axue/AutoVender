
package google.architecture.coremodel.callback;

public interface MainClickCallback {
    int ACTION_SEARCH_CLICK = 0;
    int ACTION_SHOPPING_CAR_CLICK = 1;
    int ACTION_SETTINGS_CLICK = 2;
    int ACTION_PULL_MENU = 3;
    int ACTION_PULL_MENU_BG = 4;
    void onClick(int action);
}
