
package google.architecture.coremodel.callback;


import google.architecture.coremodel.datamodel.http.entities.ShoppingCarItem;

public interface ShoppingCarItemClickCallback {
    int ACTION_ITEM_REMOVE = 0;
    int ACTION_ADD_COUNT = 1;
    int ACTION_DEL_COUNT = 2;

    void onClick(ShoppingCarItem item, int action);
}
