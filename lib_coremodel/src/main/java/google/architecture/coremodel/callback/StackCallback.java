
package google.architecture.coremodel.callback;


import android.view.View;

public interface StackCallback {
    int ACTION_BACK = 1;
    int ACTION_SETUP = 2;
    int ACTION_UNIT = 3;
    int ACTION_ROW = 4;
    int ACTION_COLUMN = 5;
    void onClick(View view, int action);
}
