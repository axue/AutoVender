
package google.architecture.coremodel.callback;


import google.architecture.coremodel.datamodel.http.entities.ProductData;

public interface ProductItemClickCallback {
    int ACTION_ITEM_CLICK = 0;
    int ACTION_BUY_CLICK = 1;
    int ACTION_SHOPPING_CAR_CLICK = 2;

    void onClick(ProductData.Product productItem, int action);
}
