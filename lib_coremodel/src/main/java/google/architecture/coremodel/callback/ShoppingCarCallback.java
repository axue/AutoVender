
package google.architecture.coremodel.callback;

public interface ShoppingCarCallback {
    int ACTION_CLEAR_SHOP_CAR = 0;
    int ACTION_PAYMENT_CLICK = 1;
    int ACTION_BACK = 2;

    void onClick(int action);
}
