package google.architecture.coremodel.datamodel.http.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import google.architecture.coremodel.datamodel.http.db.dao.ShoppingCarItemDao;
import google.architecture.coremodel.datamodel.http.entities.ShoppingCarItem;

@Database(entities = {ShoppingCarItem.class}, version = 1,exportSchema = false)
public abstract class ShoppingCarItemDatabase extends RoomDatabase {
    public abstract ShoppingCarItemDao getShoppingCarItemDao();


}
