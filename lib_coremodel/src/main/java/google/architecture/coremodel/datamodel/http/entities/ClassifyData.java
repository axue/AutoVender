package google.architecture.coremodel.datamodel.http.entities;


import java.util.List;

public class ClassifyData {

    /**
     * code : 200
     * message : success
     * data : [{"id":1,"typeName":"药品","parentId":null,"createTime":"2018-05-14 10:24:20","createUser":"admin","updateTime":null,"updateUser":null,"orderBy":null,"children":[{"id":9,"typeName":"感冒类","parentId":1,"createTime":"2018-05-17 08:49:55","createUser":"admin","updateTime":null,"updateUser":null,"orderBy":null,"children":null},{"id":5,"typeName":"解热镇痛类","parentId":1,"createTime":"2018-05-14 10:32:53","createUser":"admin","updateTime":"2018-05-17 08:52:53","updateUser":"admin","orderBy":null,"children":null}]},{"id":2,"typeName":"食品","parentId":null,"createTime":"2018-05-14 10:25:08","createUser":"admin","updateTime":null,"updateUser":null,"orderBy":null,"children":[{"id":6,"typeName":"饮料类","parentId":2,"createTime":"2018-05-14 10:34:11","createUser":"admin","updateTime":"2018-05-14 10:34:11","updateUser":null,"orderBy":null,"children":null},{"id":7,"typeName":"坚果类","parentId":2,"createTime":"2018-05-14 10:34:01","createUser":"admin","updateTime":null,"updateUser":null,"orderBy":null,"children":null}]},{"id":3,"typeName":"其他","parentId":null,"createTime":"2018-05-14 10:25:34","createUser":"admin","updateTime":null,"updateUser":null,"orderBy":null,"children":[]}]
     */

    private String code;
    private String message;
    private List<Classify> data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Classify> getData() {
        return data;
    }

    public void setData(List<Classify> data) {
        this.data = data;
    }

    public static class Classify {
        /**
         * id : 1
         * typeName : 药品
         * parentId : null
         * createTime : 2018-05-14 10:24:20
         * createUser : admin
         * updateTime : null
         * updateUser : null
         * orderBy : null
         * children : [{"id":9,"typeName":"感冒类","parentId":1,"createTime":"2018-05-17 08:49:55","createUser":"admin","updateTime":null,"updateUser":null,"orderBy":null,"children":null},{"id":5,"typeName":"解热镇痛类","parentId":1,"createTime":"2018-05-14 10:32:53","createUser":"admin","updateTime":"2018-05-17 08:52:53","updateUser":"admin","orderBy":null,"children":null}]
         */

        private int id;
        private String typeName;
        private Long parentId;
        private String createTime;
        private String createUser;
        private String updateTime;
        private String updateUser;
        private String orderBy;
        private List<Classify> children;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }

        public Object getParentId() {
            return parentId;
        }

        public void setParentId(Long parentId) {
            this.parentId = parentId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getCreateUser() {
            return createUser;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public String getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(String updateUser) {
            this.updateUser = updateUser;
        }

        public Object getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(String orderBy) {
            this.orderBy = orderBy;
        }

        public List<Classify> getChildren() {
            return children;
        }

        public void setChildren(List<Classify> children) {
            this.children = children;
        }
    }


}
