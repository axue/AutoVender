package google.architecture.coremodel.datamodel.http.db.dao;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import java.util.List;

import google.architecture.coremodel.datamodel.http.entities.ShoppingCarItem;

@Dao
public interface ShoppingCarItemDao {
    /**
     * 查询
     *
     * @return
     */
    @Query("SELECT * FROM shoppingCarItem_table order by lastModified desc")
    public List<ShoppingCarItem> getItems();

    @Query("SELECT * FROM shoppingCarItem_table WHERE id = :id")
    public List<ShoppingCarItem> getItemById(String id);


    @Query("SELECT count(*) FROM shoppingCarItem_table ")
    public Cursor getShoppingCarCount();
    /**
     * 添加
     *
     * @param items
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertItem(ShoppingCarItem... items);

    /**
     * 更新
     *
     * @param items
     */
    @Update
    public void updateItem(ShoppingCarItem... items);

    /**
     * 删除
     *
     * @param items
     */
    @Delete
    public void deleteItem(ShoppingCarItem... items);

    @Query("DELETE FROM shoppingCarItem_table")
    public void deleteAll();
}
