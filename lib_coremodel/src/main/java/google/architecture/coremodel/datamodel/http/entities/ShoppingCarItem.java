package google.architecture.coremodel.datamodel.http.entities;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;

@Entity (tableName = "shoppingCarItem_table")
public class ShoppingCarItem {
    @Ignore
    private int position;
    @PrimaryKey @NonNull
    private String id;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "thumb")
    private String thumb;
    @ColumnInfo(name = "count")
    private int count;
    @ColumnInfo(name = "creattime")
    private long creattime;
    @ColumnInfo(name = "lastModified")
    private long lastModified;
    @ColumnInfo(name = "price")
    private double price;
    @ColumnInfo(name = "command")
    private String command;
    @ColumnInfo(name = "unitNo")
    private String unitNo;
    @ColumnInfo(name = "lineNo")
    private String lineNo;
    @ColumnInfo(name = "columnNo")
    private String columnNo;
    @ColumnInfo(name = "stackCount")
    private int stackCount;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getCreattime() {
        return creattime;
    }

    public void setCreattime(long creattime) {
        this.creattime = creattime;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getLineNo() {
        return lineNo;
    }

    public void setLineNo(String lineNo) {
        this.lineNo = lineNo;
    }

    public String getColumnNo() {
        return columnNo;
    }

    public void setColumnNo(String columnNo) {
        this.columnNo = columnNo;
    }

    public boolean areItemsTheSame(ShoppingCarItem shoppingCarItem) {
        return this.getId() .equals( shoppingCarItem.getId());
    }

    public int getStackCount() {
        return stackCount;
    }

    public void setStackCount(int stackCount) {
        this.stackCount = stackCount;
    }

    public boolean areContentsTheSame(ShoppingCarItem shoppingCarItem) {
        return this.getPrice() == shoppingCarItem.getPrice()
                && this.getThumb() == shoppingCarItem.getThumb()
                && this.getTitle() == shoppingCarItem.getTitle()
                && this.getLineNo() == shoppingCarItem.getLineNo()
                && this.getLineNo() == shoppingCarItem.getLineNo()
                && this.getColumnNo() == shoppingCarItem.getColumnNo()
                && this.getCommand() == shoppingCarItem.getCommand();
    }
}
