package google.architecture.coremodel.datamodel.http.entities;



public class LoginData extends BaseData{

    private boolean data;

    public boolean isData() {
        return data;
    }

    public void setData(boolean data) {
        this.data = data;
    }
}
