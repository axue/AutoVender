package google.architecture.coremodel.datamodel.http.entities;

import android.databinding.ObservableInt;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductData {
    private int code;
    private String message;
    private List<ProductData.Product> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Product> getData() {
        return data;
    }

    public void setData(List<Product> data) {
        this.data = data;
    }

    public static class Product implements Parcelable {
        @SerializedName("drugId")
        private String id;
        @SerializedName("drugName")
        private String title;
        @SerializedName("drugPrice")
        private double price;
        @SerializedName("picUrl")
        private String thumb;
        private String drugNo;
        private String factory;
        private String originPlace;
        private String barCode;
        private String description;
        private int drugQuantity;
        private String command;
        private String unitNo;
        private String lineNo;
        private String columnNo;
        private String planId;

        protected Product(Parcel in) {
            id = in.readString();
            title = in.readString();
            price = in.readDouble();
            thumb = in.readString();
            drugNo = in.readString();
            factory = in.readString();
            originPlace = in.readString();
            barCode = in.readString();
            description = in.readString();
            drugQuantity = in.readInt();
            command = in.readString();
            unitNo = in.readString();
            lineNo = in.readString();
            columnNo = in.readString();
            planId = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(title);
            dest.writeDouble(price);
            dest.writeString(thumb);
            dest.writeString(drugNo);
            dest.writeString(factory);
            dest.writeString(originPlace);
            dest.writeString(barCode);
            dest.writeString(description);
            dest.writeInt(drugQuantity);
            dest.writeString(command);
            dest.writeString(unitNo);
            dest.writeString(lineNo);
            dest.writeString(columnNo);
            dest.writeString(planId);
        }

        public Product(){

        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Product> CREATOR = new Creator<Product>() {
            @Override
            public Product createFromParcel(Parcel in) {
                return new Product(in);
            }

            @Override
            public Product[] newArray(int size) {
                return new Product[size];
            }
        };

        public String getPlanId() {
            return planId;
        }

        public void setPlanId(String planId) {
            this.planId = planId;
        }

        public String getDrugNo() {
            return drugNo;
        }

        public void setDrugNo(String drugNo) {
            this.drugNo = drugNo;
        }

        public String getFactory() {
            return factory;
        }

        public void setFactory(String factory) {
            this.factory = factory;
        }

        public String getOriginPlace() {
            return originPlace;
        }

        public void setOriginPlace(String originPlace) {
            this.originPlace = originPlace;
        }

        public String getBarCode() {
            return barCode;
        }

        public void setBarCode(String barCode) {
            this.barCode = barCode;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public int getDrugQuantity() {
            return drugQuantity;
        }

        public void setDrugQuantity(int drugQuantity) {
            this.drugQuantity = drugQuantity;
        }

        public String getCommand() {
            return command;
        }

        public void setCommand(String command) {
            this.command = command;
        }

        public String getUnitNo() {
            return unitNo;
        }

        public void setUnitNo(String unitNo) {
            this.unitNo = unitNo;
        }

        public String getLineNo() {
            return lineNo;
        }

        public void setLineNo(String lineNo) {
            this.lineNo = lineNo;
        }

        public String getColumnNo() {
            return columnNo;
        }

        public void setColumnNo(String columnNo) {
            this.columnNo = columnNo;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public boolean areItemsTheSame(Product product) {
            return this.getDrugNo() == product.getDrugNo()
                    && this.getLineNo() == product.getLineNo()
                    && this.getLineNo() == product.getLineNo()
                    && this.getColumnNo() == product.getColumnNo();
        }

        public boolean areContentsTheSame(Product product) {
            return this.getId() == product.getId()
                    && this.getPrice() == product.getPrice()
                    && this.getThumb() == product.getThumb()
                    && this.getTitle() == product.getTitle()
                    && this.getFactory() == product.getFactory()
                    && this.getOriginPlace() == product.getOriginPlace()
                    && this.getBarCode() == product.getBarCode()
                    && this.getDescription() == product.getDescription()
                    && this.getDrugQuantity() == product.getDrugQuantity()
                    && this.getCommand() == product.getCommand();
        }


    }
}
