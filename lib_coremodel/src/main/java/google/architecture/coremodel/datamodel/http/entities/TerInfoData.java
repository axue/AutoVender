package google.architecture.coremodel.datamodel.http.entities;


public class TerInfoData {


    /**
     * code : 200
     * message : success
     * data : hfhn001
     */

    private String code;
    private String message;
    private Data data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {

        private String terNo;
        private int maxUnit;
        private int maxLine;
        private int maxColumn;
        private String dealerPhone;
        private String dealerName;
        private int operateTime;
        private int drugTime;
        private int printMaxNum;

        public String getTerNo() {
            return terNo;
        }

        public void setTerNo(String terNo) {
            this.terNo = terNo;
        }

        public int getMaxUnit() {
            return maxUnit;
        }

        public void setMaxUnit(int maxUnit) {
            this.maxUnit = maxUnit;
        }

        public int getMaxLine() {
            return maxLine;
        }

        public void setMaxLine(int maxLine) {
            this.maxLine = maxLine;
        }

        public int getMaxColumn() {
            return maxColumn;
        }

        public void setMaxColumn(int maxColumn) {
            this.maxColumn = maxColumn;
        }

        public String getDealerPhone() {
            return dealerPhone;
        }

        public void setDealerPhone(String dealerPhone) {
            this.dealerPhone = dealerPhone;
        }

        public String getDealerName() {
            return dealerName;
        }

        public void setDealerName(String dealerName) {
            this.dealerName = dealerName;
        }

        public int getOperateTime() {
            return operateTime;
        }

        public void setOperateTime(int operateTime) {
            this.operateTime = operateTime;
        }

        public int getDrugTime() {
            return drugTime;
        }

        public void setDrugTime(int drugTime) {
            this.drugTime = drugTime;
        }

        public int getPrintMaxNum() {
            return printMaxNum;
        }

        public void setPrintMaxNum(int printMaxNum) {
            this.printMaxNum = printMaxNum;
        }
    }
}
