package google.architecture.coremodel.datamodel.http.service;

import google.architecture.coremodel.datamodel.http.entities.BaseData;
import google.architecture.coremodel.datamodel.http.entities.ClassifyData;
import google.architecture.coremodel.datamodel.http.entities.LoginData;
import google.architecture.coremodel.datamodel.http.entities.ProductData;
import google.architecture.coremodel.datamodel.http.entities.OrderData;
import google.architecture.coremodel.datamodel.http.entities.TerInfoData;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by dxx on 2017/11/8.
 */

public interface AutoVenderDataService {
    @GET("api/getTerInfo")
    Observable<TerInfoData> getTerInfo(@Query("param") String deviceId);
    @POST  ("api/getDrugList")
    Observable<ProductData> getProductList(@Query("typeId") int typeId,@Query("terNo") String terNo,@Query("pageNum") int page,@Query("pageSize") int size);
    @POST  ("api/getDrugList")
    Observable<ProductData> getProductList(@Query("terNo") String terNo,@Query("pageNum") int page,@Query("pageSize") int size);
    @POST  ("api/searchDrug")
    Observable<ProductData> searchDrug(@Query("searchStr") String keywords,@Query("terNo") String terNo,@Query("pageNum") int page,@Query("pageSize") int size);
    @POST("api/getDrugType")
    Observable<ClassifyData> getClassify(@Query("terNo") String terNo);
    @FormUrlEncoded
    @POST("api/creatTrade")
    Observable<OrderData> creatTrade(@Field("terNo")String terNo, @Field("paymentType")int paymentType, @Field("drugInfos") String drugInfos);
    @POST("api/addDrug")
    Observable<BaseData> addDrug(@Query("planId") String planId, @Query("drugQuantity") int drugQuantity, @Query("operNum") int operNum );
    @POST("api/delDrug")
    Observable<BaseData> delDrug(@Query("terNo") String terNo,@Query("planId") String planId);
    @POST("api/login")
    Observable<LoginData> login(@Query("terNo") String terNo, @Query("password") String password);
    @POST  ("api/searchDrugByColumn")
    Observable<ProductData> searchDrugByColumn(@Query("terNo") String terNo,@Query("unitNo") int unitNo,@Query("lineNo") int lineNo,@Query("columnNo") int columnNo,@Query("pageNum") int page,@Query("pageSize") int size);
    @POST("api/putTerEnv")
    Observable<BaseData> putTerEnv(@Query("terNo") String terNo, @Query("temp") int temp, @Query("hum") int hum);
    @POST("api/tradeRefund")
    Observable<BaseData> tradeRefund(@Query("tradeNo") String tradeNo);
    @POST("api/saveMistake")
    Observable<BaseData> saveMistake(@Query("terNo") String terNo,@Query("errorCode") String errorCode);
}