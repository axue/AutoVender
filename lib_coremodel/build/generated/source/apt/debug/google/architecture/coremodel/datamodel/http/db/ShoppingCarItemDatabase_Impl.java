package google.architecture.coremodel.datamodel.http.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import google.architecture.coremodel.datamodel.http.db.dao.ShoppingCarItemDao;
import google.architecture.coremodel.datamodel.http.db.dao.ShoppingCarItemDao_Impl;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.util.HashMap;
import java.util.HashSet;
import javax.annotation.Generated;

@Generated("android.arch.persistence.room.RoomProcessor")
public class ShoppingCarItemDatabase_Impl extends ShoppingCarItemDatabase {
  private volatile ShoppingCarItemDao _shoppingCarItemDao;

  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `shoppingCarItem_table` (`id` TEXT NOT NULL, `title` TEXT, `thumb` TEXT, `count` INTEGER NOT NULL, `creattime` INTEGER NOT NULL, `lastModified` INTEGER NOT NULL, `price` REAL NOT NULL, `command` TEXT, `unitNo` TEXT, `lineNo` TEXT, `columnNo` TEXT, `stackCount` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"fbec9ffaff33ff9f9e97f85dde2030cc\")");
      }

      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `shoppingCarItem_table`");
      }

      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsShoppingCarItemTable = new HashMap<String, TableInfo.Column>(12);
        _columnsShoppingCarItemTable.put("id", new TableInfo.Column("id", "TEXT", true, 1));
        _columnsShoppingCarItemTable.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsShoppingCarItemTable.put("thumb", new TableInfo.Column("thumb", "TEXT", false, 0));
        _columnsShoppingCarItemTable.put("count", new TableInfo.Column("count", "INTEGER", true, 0));
        _columnsShoppingCarItemTable.put("creattime", new TableInfo.Column("creattime", "INTEGER", true, 0));
        _columnsShoppingCarItemTable.put("lastModified", new TableInfo.Column("lastModified", "INTEGER", true, 0));
        _columnsShoppingCarItemTable.put("price", new TableInfo.Column("price", "REAL", true, 0));
        _columnsShoppingCarItemTable.put("command", new TableInfo.Column("command", "TEXT", false, 0));
        _columnsShoppingCarItemTable.put("unitNo", new TableInfo.Column("unitNo", "TEXT", false, 0));
        _columnsShoppingCarItemTable.put("lineNo", new TableInfo.Column("lineNo", "TEXT", false, 0));
        _columnsShoppingCarItemTable.put("columnNo", new TableInfo.Column("columnNo", "TEXT", false, 0));
        _columnsShoppingCarItemTable.put("stackCount", new TableInfo.Column("stackCount", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysShoppingCarItemTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesShoppingCarItemTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoShoppingCarItemTable = new TableInfo("shoppingCarItem_table", _columnsShoppingCarItemTable, _foreignKeysShoppingCarItemTable, _indicesShoppingCarItemTable);
        final TableInfo _existingShoppingCarItemTable = TableInfo.read(_db, "shoppingCarItem_table");
        if (! _infoShoppingCarItemTable.equals(_existingShoppingCarItemTable)) {
          throw new IllegalStateException("Migration didn't properly handle shoppingCarItem_table(google.architecture.coremodel.datamodel.http.entities.ShoppingCarItem).\n"
                  + " Expected:\n" + _infoShoppingCarItemTable + "\n"
                  + " Found:\n" + _existingShoppingCarItemTable);
        }
      }
    }, "fbec9ffaff33ff9f9e97f85dde2030cc");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "shoppingCarItem_table");
  }

  @Override
  public ShoppingCarItemDao getShoppingCarItemDao() {
    if (_shoppingCarItemDao != null) {
      return _shoppingCarItemDao;
    } else {
      synchronized(this) {
        if(_shoppingCarItemDao == null) {
          _shoppingCarItemDao = new ShoppingCarItemDao_Impl(this);
        }
        return _shoppingCarItemDao;
      }
    }
  }
}
