package google.architecture.coremodel.datamodel.http.db.dao;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.arch.persistence.room.SharedSQLiteStatement;
import android.database.Cursor;
import google.architecture.coremodel.datamodel.http.entities.ShoppingCarItem;
import java.lang.Override;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("android.arch.persistence.room.RoomProcessor")
public class ShoppingCarItemDao_Impl implements ShoppingCarItemDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfShoppingCarItem;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfShoppingCarItem;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfShoppingCarItem;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAll;

  public ShoppingCarItemDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfShoppingCarItem = new EntityInsertionAdapter<ShoppingCarItem>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `shoppingCarItem_table`(`id`,`title`,`thumb`,`count`,`creattime`,`lastModified`,`price`,`command`,`unitNo`,`lineNo`,`columnNo`,`stackCount`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ShoppingCarItem value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getTitle());
        }
        if (value.getThumb() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getThumb());
        }
        stmt.bindLong(4, value.getCount());
        stmt.bindLong(5, value.getCreattime());
        stmt.bindLong(6, value.getLastModified());
        stmt.bindDouble(7, value.getPrice());
        if (value.getCommand() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getCommand());
        }
        if (value.getUnitNo() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getUnitNo());
        }
        if (value.getLineNo() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getLineNo());
        }
        if (value.getColumnNo() == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.getColumnNo());
        }
        stmt.bindLong(12, value.getStackCount());
      }
    };
    this.__deletionAdapterOfShoppingCarItem = new EntityDeletionOrUpdateAdapter<ShoppingCarItem>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `shoppingCarItem_table` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ShoppingCarItem value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
      }
    };
    this.__updateAdapterOfShoppingCarItem = new EntityDeletionOrUpdateAdapter<ShoppingCarItem>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `shoppingCarItem_table` SET `id` = ?,`title` = ?,`thumb` = ?,`count` = ?,`creattime` = ?,`lastModified` = ?,`price` = ?,`command` = ?,`unitNo` = ?,`lineNo` = ?,`columnNo` = ?,`stackCount` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ShoppingCarItem value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getId());
        }
        if (value.getTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getTitle());
        }
        if (value.getThumb() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getThumb());
        }
        stmt.bindLong(4, value.getCount());
        stmt.bindLong(5, value.getCreattime());
        stmt.bindLong(6, value.getLastModified());
        stmt.bindDouble(7, value.getPrice());
        if (value.getCommand() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getCommand());
        }
        if (value.getUnitNo() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getUnitNo());
        }
        if (value.getLineNo() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getLineNo());
        }
        if (value.getColumnNo() == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.getColumnNo());
        }
        stmt.bindLong(12, value.getStackCount());
        if (value.getId() == null) {
          stmt.bindNull(13);
        } else {
          stmt.bindString(13, value.getId());
        }
      }
    };
    this.__preparedStmtOfDeleteAll = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM shoppingCarItem_table";
        return _query;
      }
    };
  }

  @Override
  public void insertItem(ShoppingCarItem... items) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfShoppingCarItem.insert(items);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteItem(ShoppingCarItem... items) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfShoppingCarItem.handleMultiple(items);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateItem(ShoppingCarItem... items) {
    __db.beginTransaction();
    try {
      __updateAdapterOfShoppingCarItem.handleMultiple(items);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteAll() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAll.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAll.release(_stmt);
    }
  }

  @Override
  public List<ShoppingCarItem> getItems() {
    final String _sql = "SELECT * FROM shoppingCarItem_table order by lastModified desc";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
      final int _cursorIndexOfThumb = _cursor.getColumnIndexOrThrow("thumb");
      final int _cursorIndexOfCount = _cursor.getColumnIndexOrThrow("count");
      final int _cursorIndexOfCreattime = _cursor.getColumnIndexOrThrow("creattime");
      final int _cursorIndexOfLastModified = _cursor.getColumnIndexOrThrow("lastModified");
      final int _cursorIndexOfPrice = _cursor.getColumnIndexOrThrow("price");
      final int _cursorIndexOfCommand = _cursor.getColumnIndexOrThrow("command");
      final int _cursorIndexOfUnitNo = _cursor.getColumnIndexOrThrow("unitNo");
      final int _cursorIndexOfLineNo = _cursor.getColumnIndexOrThrow("lineNo");
      final int _cursorIndexOfColumnNo = _cursor.getColumnIndexOrThrow("columnNo");
      final int _cursorIndexOfStackCount = _cursor.getColumnIndexOrThrow("stackCount");
      final List<ShoppingCarItem> _result = new ArrayList<ShoppingCarItem>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ShoppingCarItem _item;
        _item = new ShoppingCarItem();
        final String _tmpId;
        _tmpId = _cursor.getString(_cursorIndexOfId);
        _item.setId(_tmpId);
        final String _tmpTitle;
        _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
        _item.setTitle(_tmpTitle);
        final String _tmpThumb;
        _tmpThumb = _cursor.getString(_cursorIndexOfThumb);
        _item.setThumb(_tmpThumb);
        final int _tmpCount;
        _tmpCount = _cursor.getInt(_cursorIndexOfCount);
        _item.setCount(_tmpCount);
        final long _tmpCreattime;
        _tmpCreattime = _cursor.getLong(_cursorIndexOfCreattime);
        _item.setCreattime(_tmpCreattime);
        final long _tmpLastModified;
        _tmpLastModified = _cursor.getLong(_cursorIndexOfLastModified);
        _item.setLastModified(_tmpLastModified);
        final double _tmpPrice;
        _tmpPrice = _cursor.getDouble(_cursorIndexOfPrice);
        _item.setPrice(_tmpPrice);
        final String _tmpCommand;
        _tmpCommand = _cursor.getString(_cursorIndexOfCommand);
        _item.setCommand(_tmpCommand);
        final String _tmpUnitNo;
        _tmpUnitNo = _cursor.getString(_cursorIndexOfUnitNo);
        _item.setUnitNo(_tmpUnitNo);
        final String _tmpLineNo;
        _tmpLineNo = _cursor.getString(_cursorIndexOfLineNo);
        _item.setLineNo(_tmpLineNo);
        final String _tmpColumnNo;
        _tmpColumnNo = _cursor.getString(_cursorIndexOfColumnNo);
        _item.setColumnNo(_tmpColumnNo);
        final int _tmpStackCount;
        _tmpStackCount = _cursor.getInt(_cursorIndexOfStackCount);
        _item.setStackCount(_tmpStackCount);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<ShoppingCarItem> getItemById(String id) {
    final String _sql = "SELECT * FROM shoppingCarItem_table WHERE id = ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, id);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
      final int _cursorIndexOfThumb = _cursor.getColumnIndexOrThrow("thumb");
      final int _cursorIndexOfCount = _cursor.getColumnIndexOrThrow("count");
      final int _cursorIndexOfCreattime = _cursor.getColumnIndexOrThrow("creattime");
      final int _cursorIndexOfLastModified = _cursor.getColumnIndexOrThrow("lastModified");
      final int _cursorIndexOfPrice = _cursor.getColumnIndexOrThrow("price");
      final int _cursorIndexOfCommand = _cursor.getColumnIndexOrThrow("command");
      final int _cursorIndexOfUnitNo = _cursor.getColumnIndexOrThrow("unitNo");
      final int _cursorIndexOfLineNo = _cursor.getColumnIndexOrThrow("lineNo");
      final int _cursorIndexOfColumnNo = _cursor.getColumnIndexOrThrow("columnNo");
      final int _cursorIndexOfStackCount = _cursor.getColumnIndexOrThrow("stackCount");
      final List<ShoppingCarItem> _result = new ArrayList<ShoppingCarItem>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ShoppingCarItem _item;
        _item = new ShoppingCarItem();
        final String _tmpId;
        _tmpId = _cursor.getString(_cursorIndexOfId);
        _item.setId(_tmpId);
        final String _tmpTitle;
        _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
        _item.setTitle(_tmpTitle);
        final String _tmpThumb;
        _tmpThumb = _cursor.getString(_cursorIndexOfThumb);
        _item.setThumb(_tmpThumb);
        final int _tmpCount;
        _tmpCount = _cursor.getInt(_cursorIndexOfCount);
        _item.setCount(_tmpCount);
        final long _tmpCreattime;
        _tmpCreattime = _cursor.getLong(_cursorIndexOfCreattime);
        _item.setCreattime(_tmpCreattime);
        final long _tmpLastModified;
        _tmpLastModified = _cursor.getLong(_cursorIndexOfLastModified);
        _item.setLastModified(_tmpLastModified);
        final double _tmpPrice;
        _tmpPrice = _cursor.getDouble(_cursorIndexOfPrice);
        _item.setPrice(_tmpPrice);
        final String _tmpCommand;
        _tmpCommand = _cursor.getString(_cursorIndexOfCommand);
        _item.setCommand(_tmpCommand);
        final String _tmpUnitNo;
        _tmpUnitNo = _cursor.getString(_cursorIndexOfUnitNo);
        _item.setUnitNo(_tmpUnitNo);
        final String _tmpLineNo;
        _tmpLineNo = _cursor.getString(_cursorIndexOfLineNo);
        _item.setLineNo(_tmpLineNo);
        final String _tmpColumnNo;
        _tmpColumnNo = _cursor.getString(_cursorIndexOfColumnNo);
        _item.setColumnNo(_tmpColumnNo);
        final int _tmpStackCount;
        _tmpStackCount = _cursor.getInt(_cursorIndexOfStackCount);
        _item.setStackCount(_tmpStackCount);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Cursor getShoppingCarCount() {
    final String _sql = "SELECT count(*) FROM shoppingCarItem_table ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _tmpResult = __db.query(_statement);
    return _tmpResult;
  }
}
