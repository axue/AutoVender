package com.yunqi.autovender.listener;

public interface OnClassifyListener {
    /**
     * 点击Group Item
     */
    void onGroupClick(int groupPosition);

    /**
     * 点击Child Item
     */
     void onChildClick(int groupPosition, int childPosition);
}
