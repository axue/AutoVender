package com.yunqi.autovender.adapter;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.SubMenuItemBinding;
import com.yunqi.autovender.listener.OnClassifyListener;

import java.util.List;

import google.architecture.coremodel.datamodel.http.entities.ClassifyData;


public class SubMenuAdapter extends RecyclerView.Adapter<SubMenuAdapter.SubMenuViewHolder> {

    private Context mContext;
    private List<ClassifyData.Classify> mClassifies;
    private OnClassifyListener mOnClassifyListener;
    private int mGroupPosition = 0;
    @NonNull
    @Override
    public SubMenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SubMenuItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.sub_menu_item,
                        parent, false);
        return new SubMenuViewHolder(binding);
    }

    public SubMenuAdapter(Context context, List<ClassifyData.Classify> classifies) {
        mContext = context;
        mClassifies=classifies;
    }
    public void setOnClassifyListener(OnClassifyListener onClassifyListener){
        mOnClassifyListener=onClassifyListener;
    }

    public void setGroupPosition(int groupPosition) {
        mGroupPosition = groupPosition;
    }

    @Override
    public void onBindViewHolder(@NonNull SubMenuViewHolder holder, int position) {
        holder.binding.setClassify(mClassifies.get(position));
        holder.binding.executePendingBindings();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnClassifyListener != null)
                    mOnClassifyListener.onChildClick(mGroupPosition,position);
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull SubMenuViewHolder holder, int position,@NonNull List<Object> payloads) {
        onBindViewHolder(holder,position);
    }

    @Override
    public int getItemCount() {
        return mClassifies==null?0:mClassifies.size();
    }

    static class SubMenuViewHolder extends RecyclerView.ViewHolder {
        SubMenuItemBinding binding;

        public SubMenuViewHolder(SubMenuItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
