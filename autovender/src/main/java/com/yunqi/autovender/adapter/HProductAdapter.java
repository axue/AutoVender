package com.yunqi.autovender.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.yunqi.autovender.R;
import google.architecture.coremodel.callback.ProductItemClickCallback;
import com.yunqi.autovender.databinding.ProductItemBinding;
import com.yunqi.autovender.view.PageGridView;

import java.util.ArrayList;
import java.util.List;

import google.architecture.coremodel.datamodel.http.entities.ProductData;
/**
 * 横屏的ProductAdapter
 */
public class HProductAdapter extends PageGridView.PagingAdapter<RecyclerView.ViewHolder>  {
    List<ProductData.Product> productList=new ArrayList<>();
    ProductItemClickCallback productItemClickCallback;


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ProductItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.product_item,
                        parent, false);
        binding.setCallback(productItemClickCallback);
        return new ProductViewHolder(binding);
    }

    public HProductAdapter(@Nullable ProductItemClickCallback itemClickCallback, List<ProductData.Product> productList) {
        productItemClickCallback = itemClickCallback;
        this.productList=productList;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof HProductAdapter.ProductViewHolder) {
            HProductAdapter.ProductViewHolder hHolder=(HProductAdapter.ProductViewHolder)holder;
            hHolder.binding.setProductItem(productList.get(position));
            hHolder.binding.executePendingBindings();
        }
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            if(holder instanceof HProductAdapter.ProductViewHolder) {
                HProductAdapter.ProductViewHolder hHolder=(HProductAdapter.ProductViewHolder)holder;
                String payload = (String) payloads.get(0);
                if (TextUtils.equals(payload, "updateStack")) {
                    hHolder.binding.setProductItem(productList.get(position));
                    hHolder.binding.executePendingBindings();
                }
            }
        }
    }

    @Override
    public int getItemCount() {
         return productList == null ? 0 : productList.size();
    }

    @Override
    public List getData() {
        return productList;
    }

    @Override
    public Object getEmpty() {
        return "";
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {
        ProductItemBinding binding;
        public ProductViewHolder(ProductItemBinding binding ) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
