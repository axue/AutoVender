package com.yunqi.autovender.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yunqi.autovender.R;

import java.util.List;

import google.architecture.coremodel.datamodel.http.entities.ClassifyData;

public class MenuAdapter extends BaseExpandableListAdapter {

    private ClassifyData mClassifyData;
    private Context mContext;
    private int mGroupPosition = 0;
    private int mChildPosition = -1;

    public MenuAdapter(Context context, ClassifyData classifyData) {
        mContext = context;
        mClassifyData = classifyData;
    }

    public int getGroupPosition() {
        return mGroupPosition;
    }


    public int getChildPosition() {
        return mChildPosition;
    }

    public void setGroupPosition(int groupPosition) {
        mGroupPosition = groupPosition;
        notifyDataSetChanged();
    }

    public void setChildPosition(int childPosition) {
        mChildPosition = childPosition;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        if (mClassifyData == null) {
            return 0;
        }
        List<ClassifyData.Classify> groupClassifies = mClassifyData.getData();
        if (groupClassifies == null) {
            return 0;
        }
        return groupClassifies.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (mClassifyData == null) {
            return 0;
        }
        List<ClassifyData.Classify> groupClassifies = mClassifyData.getData();
        if (groupClassifies == null) {
            return 0;
        }
        ClassifyData.Classify groupClassify = groupClassifies.get(groupPosition);
        if (groupClassify == null) {
            return 0;
        }
        List<ClassifyData.Classify> childClassifies = groupClassify.getChildren();
        if (childClassifies == null) {
            return 0;
        }
        return childClassifies.size();
    }

    public void setGroupAndChildPosition(int groupPosition,int childPosition) {
        mGroupPosition = groupPosition;
        mChildPosition = childPosition;
        notifyDataSetChanged();
    }

    @Override
    public ClassifyData.Classify getGroup(int groupPosition) {
        if (mClassifyData == null) {
            return null;
        }
        List<ClassifyData.Classify> groupClassifies = mClassifyData.getData();
        if (groupClassifies == null) {
            return null;
        }
        return groupClassifies.get(groupPosition);
    }

    @Override
    public ClassifyData.Classify getChild(int groupPosition, int childPosition) {
        if (mClassifyData == null) {
            return null;
        }
        List<ClassifyData.Classify> groupClassifies = mClassifyData.getData();
        if (groupClassifies == null) {
            return null;
        }
        ClassifyData.Classify groupClassify = groupClassifies.get(groupPosition);
        if (groupClassify == null) {
            return null;
        }
        List<ClassifyData.Classify> childClassifies = groupClassify.getChildren();
        if (childClassifies == null) {
            return null;
        }
        return childClassifies.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder groupViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.group_item, parent, false);
            groupViewHolder = new GroupViewHolder();
            groupViewHolder.tvTitle = (TextView) convertView.findViewById(R.id.txt_title);
            groupViewHolder.groupItem = (RelativeLayout) convertView.findViewById(R.id.group_item);
            convertView.setTag(groupViewHolder);
        } else {
            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }
        if (mGroupPosition == groupPosition) {
            groupViewHolder.groupItem.setBackgroundResource(R.drawable.group_item_pre);
            groupViewHolder.tvTitle.setTextColor(mContext.getResources().getColor(R.color.text_pre));
        } else {
            groupViewHolder.groupItem.setBackgroundResource(R.drawable.white);
            groupViewHolder.tvTitle.setTextColor(mContext.getResources().getColor(R.color.text_normal));
        }
        ClassifyData.Classify group = getGroup(groupPosition);
        groupViewHolder.tvTitle.setText(group.getTypeName());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder childViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.child_item, parent, false);
            childViewHolder = new ChildViewHolder();
            childViewHolder.tvTitle = (TextView) convertView.findViewById(R.id.txt_title);
            convertView.setTag(childViewHolder);
        } else {
            childViewHolder = (ChildViewHolder) convertView.getTag();
        }
        if (mGroupPosition == groupPosition && mChildPosition == childPosition) {
            childViewHolder.tvTitle.setTextColor(mContext.getResources().getColor(R.color.white));
            childViewHolder.tvTitle.setBackgroundResource(R.mipmap.bg_lv2);
        } else {
            childViewHolder.tvTitle.setTextColor(mContext.getResources().getColor(R.color.text_normal));
            childViewHolder.tvTitle.setBackgroundResource(R.drawable.transparent);
        }
        ClassifyData.Classify child = getChild(groupPosition, childPosition);
        childViewHolder.tvTitle.setText(child.getTypeName());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class GroupViewHolder {
        RelativeLayout groupItem;
        TextView tvTitle;
    }

    static class ChildViewHolder {
        TextView tvTitle;
    }
}
