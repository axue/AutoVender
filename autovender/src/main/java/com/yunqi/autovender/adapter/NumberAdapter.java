package com.yunqi.autovender.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.NumberItemBinding;
import com.yunqi.autovender.view.ConfirmDialog;

import java.util.List;


public class NumberAdapter extends RecyclerView.Adapter<NumberAdapter.NumberViewHolder> {

    private Context mContext;
    private int mMaxNumber;
    private int mCurNumber;

    @NonNull
    @Override
    public NumberViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        NumberItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.number_item,
                        parent, false);
        return new NumberViewHolder(binding);
    }

    public NumberAdapter(Context context, int maxNumber, int curNumber) {
        mContext = context;
        mMaxNumber = maxNumber;
        mCurNumber = curNumber;
    }

    public void setCurNumber(int curNumber) {
        mCurNumber = curNumber;
        notifyItemChanged(mCurNumber, "highlight");
    }

    public void resetLastNumber() {
        notifyItemChanged(mCurNumber, "normal");
    }


    public int getCurNumber() {
        return mCurNumber;
    }

    @Override
    public void onBindViewHolder(@NonNull NumberViewHolder holder, int position) {
        String number = position == 0 ? "全部" : String.valueOf(position);
        holder.binding.setNumber(number);
        holder.binding.executePendingBindings();
        if (position == mCurNumber) {
            holder.binding.btnItem.setBackgroundResource(R.drawable.common_btn_bg3);
            holder.binding.btnItem.setTextColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.binding.btnItem.setBackgroundResource(R.drawable.common_btn_bg8);
            holder.binding.btnItem.setTextColor(mContext.getResources().getColor(R.color.text_color4));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull NumberViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            String payload = (String) payloads.get(0);
            if (TextUtils.equals(payload, "highlight")) {
                holder.binding.btnItem.setBackgroundResource(R.drawable.common_btn_bg3);
                holder.binding.btnItem.setTextColor(mContext.getResources().getColor(R.color.white));
            } else if (TextUtils.equals(payload, "normal")) {
                holder.binding.btnItem.setBackgroundResource(R.drawable.common_btn_bg8);
                holder.binding.btnItem.setTextColor(mContext.getResources().getColor(R.color.text_color4));
            }
        }
        holder.binding.btnItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == mCurNumber) {
                    return;
                }
                resetLastNumber();
                setCurNumber(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMaxNumber + 1;
    }

    static class NumberViewHolder extends RecyclerView.ViewHolder {
        NumberItemBinding binding;

        public NumberViewHolder(NumberItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
