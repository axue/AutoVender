package com.yunqi.autovender.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.ShoppingCarItemBinding;
import com.yunqi.autovender.model.ShoppingCarInfo;

import java.util.List;

import google.architecture.coremodel.callback.ShoppingCarItemClickCallback;
import google.architecture.coremodel.datamodel.http.entities.ShoppingCarItem;

public class ShoppingCarAdapter extends RecyclerView.Adapter<ShoppingCarAdapter.ShoppingCarItemViewHolder>  {
    List<ShoppingCarItem> shoppingCarItems;
    ShoppingCarItemClickCallback shoppingCarItemClickCallback;


    @NonNull
    @Override
    public ShoppingCarItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ShoppingCarItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.shopping_car_item,
                        parent, false);
        binding.setCallback(shoppingCarItemClickCallback);
        return new ShoppingCarItemViewHolder(binding);
    }

    public ShoppingCarAdapter(@Nullable ShoppingCarItemClickCallback itemClickCallback) {
        shoppingCarItemClickCallback = itemClickCallback;
    }

    @Override
    public void onBindViewHolder(@NonNull ShoppingCarItemViewHolder holder, int position) {
        ShoppingCarItem item=shoppingCarItems.get(position);
        ShoppingCarInfo shoppingCarInfo=new ShoppingCarInfo();
        shoppingCarInfo.addEnable.set(item.getCount()<item.getStackCount());
        shoppingCarInfo.reduceEnable.set(item.getCount()>1);
        holder.binding.setShoppingCarInfo(shoppingCarInfo);
        item.setPosition(position);
        holder.binding.setShoppingCarItem(item);
        holder.binding.executePendingBindings();
    }

    @Override
    public void onBindViewHolder(@NonNull ShoppingCarItemViewHolder holder, int position, @NonNull List<Object> payloads) {
        if(payloads.isEmpty()){
            onBindViewHolder(holder,position);
        }
        else {
            ShoppingCarItem item=shoppingCarItems.get(position);
            holder.binding.txtCount.setText(String.valueOf(item.getCount()));
            ShoppingCarInfo shoppingCarInfo=new ShoppingCarInfo();
            shoppingCarInfo.addEnable.set(item.getCount()<item.getStackCount());
            shoppingCarInfo.reduceEnable.set(item.getCount()>1);
            holder.binding.setShoppingCarInfo(shoppingCarInfo);
        }
    }

    public void clear(){
        shoppingCarItems.clear();
        notifyDataSetChanged();
    }

    public void removeData(int position) {
        shoppingCarItems.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position,shoppingCarItems.size()-position);//通知数据与界面重新绑定
    }

    public void updateData(int position) {
        notifyItemChanged(position,1);
    }

    @Override
    public int getItemCount() {
         return shoppingCarItems == null ? 0 : shoppingCarItems.size();
    }

    static class ShoppingCarItemViewHolder extends RecyclerView.ViewHolder {
        ShoppingCarItemBinding binding;
        public ShoppingCarItemViewHolder(ShoppingCarItemBinding binding ) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public void setShoppingCarItemList(final List<ShoppingCarItem> list){
        if(shoppingCarItems == null){
            shoppingCarItems = list;
            notifyItemRangeInserted(0, shoppingCarItems.size());
        }else {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return shoppingCarItems.size();
                }

                @Override
                public int getNewListSize() {
                    return list.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    ShoppingCarItem oldData = shoppingCarItems.get(oldItemPosition);
                    ShoppingCarItem newData = list.get(newItemPosition);
                    return oldData.areItemsTheSame(newData);
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    ShoppingCarItem oldData = shoppingCarItems.get(oldItemPosition);
                    ShoppingCarItem newData = list.get(newItemPosition);
                    return oldData.areContentsTheSame(newData);
                }
            });
            shoppingCarItems = list;
            diffResult.dispatchUpdatesTo(this);
        }
    }
}
