package com.yunqi.autovender.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.yunqi.autovender.R;

import java.util.ArrayList;
import java.util.List;

import google.architecture.coremodel.callback.StackItemClickCallback;
import google.architecture.coremodel.datamodel.http.entities.ProductData;

import com.yunqi.autovender.databinding.StackItemBinding;

public class StackAdapter extends RecyclerView.Adapter<StackAdapter.StackViewHolder> {
    List<ProductData.Product> productList = new ArrayList<>();
    StackItemClickCallback stackItemClickCallback;

    @NonNull
    @Override
    public StackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        StackItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.stack_item,
                        parent, false);
        binding.setCallback(stackItemClickCallback);
        return new StackViewHolder(binding);
    }

    public StackAdapter(@Nullable StackItemClickCallback itemClickCallback) {
        stackItemClickCallback = itemClickCallback;
    }

    @Override
    public void onBindViewHolder(StackViewHolder holder, int position) {
        holder.binding.setProductItem(productList.get(position));
        holder.binding.setPosition(position);
        holder.binding.executePendingBindings();
    }

    @Override
    public void onBindViewHolder(StackViewHolder holder, int position, List<Object> payloads) {
        //payloads为空 即不是调用notifyItemChanged(position,payloads)方法执行的
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position - 1);
        } else {
            int newDrugQuantity = productList.get(position - 1).getDrugQuantity();
            holder.binding.txtStack.setText(String.valueOf(newDrugQuantity));
            if (holder.binding.imgOffline.isClickable()) {
                if (newDrugQuantity == 0) {
                    holder.binding.imgOffline.setClickable(false);
                    holder.binding.imgOffline.setAlpha(0.6f);
                }
            } else {
                if (newDrugQuantity > 0) {
                    holder.binding.imgOffline.setClickable(true);
                    holder.binding.imgOffline.setAlpha(1.0f);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    static class StackViewHolder extends RecyclerView.ViewHolder {
        StackItemBinding binding;

        public StackViewHolder(StackItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


    public void addProductList(final List<ProductData.Product> list, boolean isAddMore) {
        if (!isAddMore)
            productList.clear();
        productList.addAll(list);
        notifyDataSetChanged();
    }

    public List<ProductData.Product> getProductList() {
        return productList;
    }


}
