package com.yunqi.autovender.adapter;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.PullMenuItemBinding;
import com.yunqi.autovender.listener.OnClassifyListener;

import java.util.List;

import google.architecture.coremodel.datamodel.http.entities.ClassifyData;


public class PullMenuAdapter extends RecyclerView.Adapter<PullMenuAdapter.PullMenuViewHolder> {

    private Context mContext;
    private int mGroupPosition = 0;
    private List<ClassifyData.Classify> mClassifies;
    private OnClassifyListener mOnClassifyListener;

    @NonNull
    @Override
    public PullMenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PullMenuItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.pull_menu_item,
                        parent, false);
        return new PullMenuViewHolder(binding);
    }

    public PullMenuAdapter(Context context, List<ClassifyData.Classify> classifies) {
        mContext = context;
        mClassifies = classifies;
    }

    public void setGroupPosition(int groupPosition) {
        mGroupPosition = groupPosition;
        notifyDataSetChanged();
    }

    public void setOnClassifyListener(OnClassifyListener onClassifyListener){
        mOnClassifyListener=onClassifyListener;
    }


    @Override
    public void onBindViewHolder(@NonNull PullMenuViewHolder holder, int position) {
        holder.binding.setClassify(mClassifies.get(position));
        holder.binding.executePendingBindings();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnClassifyListener != null)
                    mOnClassifyListener.onGroupClick(position);
            }
        });
        if(mGroupPosition==position){
            holder.binding.layoutRoot.setBackgroundColor(mContext.getResources().getColor(R.color.bg_color2));
        }
        else {
            holder.binding.layoutRoot.setBackgroundColor(mContext.getResources().getColor(R.color.transparent));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull PullMenuViewHolder holder, int position, @NonNull List<Object> payloads) {
        onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return mClassifies == null ? 0 : mClassifies.size();
    }

    static class PullMenuViewHolder extends RecyclerView.ViewHolder {
        PullMenuItemBinding binding;

        public PullMenuViewHolder(PullMenuItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


}
