package com.yunqi.autovender;



import com.alibaba.android.arouter.launcher.ARouter;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.tencent.bugly.Bugly;

import cn.jpush.android.api.JPushInterface;
import google.architecture.common.base.BaseApplication;

public class AutoVenderApp extends BaseApplication {
    private static final String TAG = "AutoVenderApp";

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        ARouter.init(this);
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);
        Bugly.init(getApplicationContext(), getString(R.string.bugly_appid), false);
    }
}
