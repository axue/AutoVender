package com.yunqi.autovender.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.yunqi.autovender.view.FloatView;

/**
 * 监听服务用于和下位机通信的服务
 */
public class ObserverService extends Service {
    FloatView mFloatView;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mFloatView = new FloatView(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        showFloat();
        return super.onStartCommand(intent, flags, startId);
    }

    public void showFloat() {
        if ( mFloatView != null ) {
            mFloatView.show();
        }
    }

    public void hideFloat() {
        if ( mFloatView != null ) {
            mFloatView.hide();
        }
    }

    public void destroyFloat() {
        if ( mFloatView != null ) {
            mFloatView.destroy();
        }
        mFloatView = null;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyFloat();
    }

}
