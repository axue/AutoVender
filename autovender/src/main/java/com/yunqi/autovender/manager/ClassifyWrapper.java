package com.yunqi.autovender.manager;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ExpandableListView;

import com.yunqi.autovender.adapter.MenuAdapter;
import com.yunqi.autovender.adapter.PullMenuAdapter;
import com.yunqi.autovender.listener.OnClassifyListener;


/**
 * 主界面的Classify的Wrapper类：主要用于横向使用ExpandableListView，纵向使用RecyclerView的包装类
 */
public class ClassifyWrapper {

    public static final int TYPE_EXPANDABLELISTVIEW = 0;
    public static final int TYPE_RECYCLERVIEW = 1;
    private Context mContext;
    private View mClassifyView;
    private OnClassifyListener mOnClassifyListener;
    private int mType = TYPE_EXPANDABLELISTVIEW;
    private int mGroupCount = 0;

    public ClassifyWrapper(Context context, View classifyView) {
        mContext = context;
        mClassifyView = classifyView;
    }

    public void setOnClassifyListener(OnClassifyListener onClassifyListener) {
        mOnClassifyListener = onClassifyListener;
    }


    public void initClassify() {
        if (mClassifyView instanceof ExpandableListView) {
            mType = TYPE_EXPANDABLELISTVIEW;
            ExpandableListView listviewNavi = (ExpandableListView) mClassifyView;
            listviewNavi.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                @Override
                public void onGroupExpand(int groupPosition) {
                    // TODO Auto-generated method stub
                    for (int i = 0; i < mGroupCount; i++) {
                        if (groupPosition != i) {
                            listviewNavi.collapseGroup(i);
                        }
                    }
                }
            });
            listviewNavi.setOnGroupClickListener(mOnGroupClickListener);
            listviewNavi.setOnChildClickListener(mOnChildClickListener);
        } else if (mClassifyView instanceof RecyclerView) {
            mType = TYPE_RECYCLERVIEW;
        }
    }

    public int getType() {
        return mType;
    }

    /**
     * 设置横向屏幕的Adapter
     */
    public void setAdapter(MenuAdapter menuAdapter) {
        mGroupCount = menuAdapter.getGroupCount();
        ExpandableListView listviewNavi = (ExpandableListView) mClassifyView;
        listviewNavi.setAdapter(menuAdapter);
    }

    /**
     * 设置纵向屏幕的Adapter
     */
    public void setAdapter(PullMenuAdapter pullMenuAdapter) {
        RecyclerView listviewNavi = (RecyclerView) mClassifyView;
        listviewNavi.setLayoutManager(new LinearLayoutManager(mContext));
        listviewNavi.setAdapter(pullMenuAdapter);
    }

    public void collapseGroup(int groupPosition) {
        if (mType == ClassifyWrapper.TYPE_EXPANDABLELISTVIEW) {
            ExpandableListView listviewNavi = (ExpandableListView) mClassifyView;
            listviewNavi.collapseGroup(groupPosition);
        }
    }


    final private ExpandableListView.OnChildClickListener mOnChildClickListener = new ExpandableListView.OnChildClickListener() {


        @Override
        public boolean onChildClick(ExpandableListView parent, View v,
                                    int groupPosition, int childPosition, long id) {
            if (mOnClassifyListener != null)
                mOnClassifyListener.onChildClick(groupPosition, childPosition);
            // TODO 执行选中后的操作
            return true;
        }
    };
    final private ExpandableListView.OnGroupClickListener mOnGroupClickListener = new ExpandableListView.OnGroupClickListener() {


        @Override
        public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
            if (mOnClassifyListener != null)
                mOnClassifyListener.onGroupClick(groupPosition);
            return false;
        }
    };



}
