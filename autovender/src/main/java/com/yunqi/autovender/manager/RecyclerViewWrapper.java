package com.yunqi.autovender.manager;


import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import google.architecture.common.widget.xrecyclerview.XRecyclerView;
import xiao.free.horizontalrefreshlayout.HorizontalRefreshLayout;
import xiao.free.horizontalrefreshlayout.RefreshCallBack;
import xiao.free.horizontalrefreshlayout.refreshhead.LoadingRefreshFooter;
import xiao.free.horizontalrefreshlayout.refreshhead.LoadingRefreshHeader;

/**
 * 主界面的RecyclerView的Wrapper类：主要用于封装横向、纵向的滑动接口
        */
public class RecyclerViewWrapper {

    private Context mContext;
    private RefreshListener mRefreshListener;
    private RecyclerView mContentRecyclerView;
    private View mRefreshView;
    public RecyclerViewWrapper (Context context,View recyclerView){
        mContext=context;
        mContentRecyclerView=(RecyclerView)recyclerView;
    }
    public void setRefreshListener(RefreshListener refreshListener){
        mRefreshListener=refreshListener;
    }

    public void setRefreshEnabled(boolean refreshEnabled) {
        if(mRefreshView instanceof HorizontalRefreshLayout ){
            HorizontalRefreshLayout refreshLayout=(HorizontalRefreshLayout)  mRefreshView;
            refreshLayout.setPullRefreshEnabled(refreshEnabled);
        }
        else if(mRefreshView instanceof XRecyclerView){
            XRecyclerView refreshLayout=(XRecyclerView)  mRefreshView;
            refreshLayout.setPullRefreshEnabled(refreshEnabled);
        }
    }

    public void setLoadingMoreEnabled(boolean loadingMoreEnabled) {
        if(mRefreshView instanceof HorizontalRefreshLayout ){
            HorizontalRefreshLayout refreshLayout=(HorizontalRefreshLayout)  mRefreshView;
            refreshLayout.setLoadingMoreEnabled(loadingMoreEnabled);
        }
        else if(mRefreshView instanceof XRecyclerView){
            XRecyclerView refreshLayout=(XRecyclerView)  mRefreshView;
            refreshLayout.setLoadingMoreEnabled(loadingMoreEnabled);
        }
    }


    /**
     * 刷新完成
     */
    public void onRefreshComplete() {
        if(mRefreshView instanceof HorizontalRefreshLayout ){
            HorizontalRefreshLayout refreshLayout=(HorizontalRefreshLayout)  mRefreshView;
            refreshLayout.onRefreshComplete();
        }
        else if(mRefreshView instanceof XRecyclerView){
            XRecyclerView refreshLayout=(XRecyclerView)  mRefreshView;
            refreshLayout.refreshComplete();
        }
    }


    public void initRefreshView(View refreshView){
        mRefreshView=refreshView;
        if(refreshView instanceof HorizontalRefreshLayout ){
            HorizontalRefreshLayout refreshLayout=(HorizontalRefreshLayout)  refreshView;
            refreshLayout.setRefreshHeader(new LoadingRefreshHeader(mContext), HorizontalRefreshLayout.LEFT);
            refreshLayout.setRefreshHeader(new LoadingRefreshFooter(mContext), HorizontalRefreshLayout.RIGHT);
            refreshLayout.setLoadingMoreEnabled(false);
            refreshLayout.setRefreshCallback(new RefreshCallBack() {
                @Override
                public void onLeftRefreshing() {
                    if(mRefreshListener!=null)
                        mRefreshListener.onRefresh();
                }

                @Override
                public void onRightRefreshing() {
                    if(mRefreshListener!=null)
                        mRefreshListener.onLoadMore();
                }
            });
        }
        else if(refreshView instanceof XRecyclerView){
            XRecyclerView refreshLayout=(XRecyclerView)  refreshView;
            refreshLayout.setLayoutManager(new GridLayoutManager(mContext, 2));
            refreshLayout.setLoadingMoreEnabled(false);
            refreshLayout.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                if(mRefreshListener!=null)
                    mRefreshListener.onRefresh();
            }

            @Override
            public void onLoadMore() {
                if(mRefreshListener!=null)
                    mRefreshListener.onLoadMore();
            }
        });
        }
    }

    public void setAdapter(RecyclerView.Adapter adapter){
        mContentRecyclerView.setAdapter(adapter);
    }
    public interface RefreshListener {
        /**
         * 刷新
         */
        void onRefresh();
        /**
         * 加载更多
         */
        void onLoadMore();
    }

}
