package com.yunqi.autovender.model;

import java.util.HashMap;
import java.util.Map;

public class GlobalData {
    private static  GlobalData mGlobalData;
    private Map<String,Object> mDatas;
    public static GlobalData getInstance(){
        if(mGlobalData==null){
            mGlobalData=new GlobalData();
        }
        return mGlobalData;
    }
    private GlobalData(){
        mDatas=new HashMap<>();
    }
    public void putData (String key,Object data){
        mDatas.put(key,data);
    }
    public Object getData (String key){
       return mDatas.get(key);
    }
}
