package com.yunqi.autovender.model;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;

/**
 * Created by ghcui-pc on 2018/7/16.
 */

public class StackInfo {
    public final ObservableInt count = new ObservableInt();
    public final ObservableBoolean reduceEnable = new ObservableBoolean();
    public final ObservableBoolean addEnable = new ObservableBoolean();
    public final ObservableBoolean hasModify = new ObservableBoolean();
}
