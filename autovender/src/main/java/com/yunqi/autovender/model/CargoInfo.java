package com.yunqi.autovender.model;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;

/**
 * 货道信息
 * Created by ghcui-pc on 2018/7/16.
 */

public class CargoInfo {
    public int maxUnit;
    public int maxRow;
    public int maxColumn;

    public CargoInfo(int maxUnit,int maxRow,int maxColumn){
        this.maxUnit=maxUnit;
        this.maxRow=maxRow;
        this.maxColumn=maxColumn;
    }
}
