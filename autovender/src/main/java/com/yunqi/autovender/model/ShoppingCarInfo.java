package com.yunqi.autovender.model;

import android.databinding.ObservableBoolean;

public class ShoppingCarInfo {
    public final ObservableBoolean addEnable = new ObservableBoolean();
    public final ObservableBoolean reduceEnable = new ObservableBoolean();
}
