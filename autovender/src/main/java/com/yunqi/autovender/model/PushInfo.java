package com.yunqi.autovender.model;

import java.util.List;

/**
 * 推送的Info
 * Created by ghcui-pc on 2018/7/16.
 */

public class PushInfo {


    /**
     * drugInfos : [{"drugName":"金嗓清音丸(碑林)","planId":"ahhn002,1,3,7","quantity":"1","drugPrice":"0.01","totalPrice":"0.01","command":"AA010307B5"}]
     * code : 10001
     * tradeNo : 5a253e470b0737c9a2e48c6a92dbacae
     * status : success
     */

    private int code;
    private String tradeNo;
    private String status;
    private List<DrugInfosBean> drugInfos;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DrugInfosBean> getDrugInfos() {
        return drugInfos;
    }

    public void setDrugInfos(List<DrugInfosBean> drugInfos) {
        this.drugInfos = drugInfos;
    }

    public static class DrugInfosBean {
        /**
         * drugName : 金嗓清音丸(碑林)
         * planId : ahhn002,1,3,7
         * quantity : 1
         * drugPrice : 0.01
         * totalPrice : 0.01
         * command : AA010307B5
         */

        private String drugName;
        private String planId;
        private int quantity;
        private double drugPrice;
        private double totalPrice;
        private String command;

        public String getDrugName() {
            return drugName;
        }

        public void setDrugName(String drugName) {
            this.drugName = drugName;
        }

        public String getPlanId() {
            return planId;
        }

        public void setPlanId(String planId) {
            this.planId = planId;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public double getDrugPrice() {
            return drugPrice;
        }

        public void setDrugPrice(double drugPrice) {
            this.drugPrice = drugPrice;
        }

        public double getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(double totalPrice) {
            this.totalPrice = totalPrice;
        }

        public String getCommand() {
            return command;
        }

        public void setCommand(String command) {
            this.command = command;
        }
    }
}
