package com.yunqi.autovender.model;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

public class SetupInfo {
    //服务器请求Url
    public ObservableField<String> serverUrl = new ObservableField<>();
    //串口路径
    public ObservableField<String> serialPath = new ObservableField<>();
    //串口端口号
    public ObservableField<String> serialPort = new ObservableField<>();
    //下位机下货超时时长
    public ObservableField<String> timeOut = new ObservableField<>();
    //倒计时时长
    public ObservableField<String> countDownTime = new ObservableField<>();
    //小票打印最大长度
    public ObservableField<String> printMaxLength = new ObservableField<>();
    public final ObservableBoolean needSave = new ObservableBoolean();
    //是否是自动获取配置，首页是自动获取配置，商品管理不是自动获取
    public final ObservableBoolean autoConfig = new ObservableBoolean();


    public SetupInfo(){
        serverUrl.set("");
        serialPath.set("");
        serialPort.set("");
        timeOut.set("");
        countDownTime.set("");
        printMaxLength.set("");
        needSave.set(false);
        autoConfig.set(false);
    }
}
