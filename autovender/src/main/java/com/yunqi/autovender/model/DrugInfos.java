package com.yunqi.autovender.model;

import java.util.List;

public class DrugInfos {

    private List<DrugInfo> drugInfos;

    public List<DrugInfo> getDrugInfos() {
        return drugInfos;
    }

    public void setDrugInfos(List<DrugInfo> drugInfos) {
        this.drugInfos = drugInfos;
    }
}
