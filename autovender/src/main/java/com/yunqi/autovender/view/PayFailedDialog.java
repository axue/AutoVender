package com.yunqi.autovender.view;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.view.View;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.DialogPayFailedBinding;


/**
 * 出货失败Dialog
 */
public abstract class PayFailedDialog extends Dialog {
    public Context mContext;
    private MyCountDownTimer mCountDownTimer;
    private String mPhone;
    public PayFailedDialog(@NonNull Context context,String phone) {
        this(context,R.style.MyDialog);
        mContext=context;
        this.mPhone=phone;
    }

    private PayFailedDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(mContext, R.layout.dialog_pay_failed, null);
        DialogPayFailedBinding bind = DataBindingUtil.bind(view);
        bind.setPhone(mPhone);
        setContentView(view);
        setCanceledOnTouchOutside(false);
    }




    @Override
    public void show() {
        super.show();
        startCountDown();
    }



    /**
     * 开始倒计时
     */
    private void startCountDown(){
        mCountDownTimer= new MyCountDownTimer(5000, 1000);
        mCountDownTimer.start();
    }

    /**
     * 停止倒计时
     */
    public void stopCountDown(){
        if(mCountDownTimer!=null){
            mCountDownTimer.cancel();
        }
    }



    class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            dismiss();
            onStopCountDown();
        }
    }
    protected abstract void onStopCountDown();
}
