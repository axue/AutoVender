package com.yunqi.autovender.view;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.view.View;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.DialogPaySuccessBinding;

import google.architecture.coremodel.callback.ConfirmCallback;

/**
 * 出货成功Dialog
 */
public class PaySuccessDialog extends Dialog {
    public Context mContext;
    private DialogPaySuccessBinding mBinding;
    private MyCountDownTimer mCountDownTimer;
    public PaySuccessDialog(@NonNull Context context) {
        this(context,R.style.MyDialog);
        mContext=context;
    }

    private PaySuccessDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(mContext, R.layout.dialog_pay_success, null);
        mBinding = DataBindingUtil.bind(view);
        setContentView(view);
        setCanceledOnTouchOutside(false);
        mBinding.setCallback(mConfirmCallback);
    }


    private ConfirmCallback mConfirmCallback = new ConfirmCallback() {

        @Override
        public void onClick(int action) {
            stopCountDown();
            dismiss();
        }
    };


    @Override
    public void show() {
        super.show();
        startCountDown();
    }



    /**
     * 开始倒计时
     */
    private void startCountDown(){
        mCountDownTimer= new MyCountDownTimer(5000, 1000);
        mCountDownTimer.start();
    }

    /**
     * 停止倒计时
     */
    public void stopCountDown(){
        if(mCountDownTimer!=null){
            mCountDownTimer.cancel();
        }
    }



    class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {



        }

        @Override
        public void onFinish() {
            dismiss();
        }
    }

}
