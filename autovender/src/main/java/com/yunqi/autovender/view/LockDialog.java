package com.yunqi.autovender.view;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.DialogLockBinding;

import google.architecture.common.util.EncryptUtils;
import google.architecture.common.util.TimeUtil;
import google.architecture.common.util.ToastUtils;
import google.architecture.coremodel.callback.LockCallback;

/**
 * 锁屏、解锁Dialog
 */
public abstract class LockDialog extends Dialog {
    private static final String TAG = "LockDialog";
    public static final int STATUS_LOCKED = 0;//锁屏状态
    public static final int STATUS_UNLOCKING = 1;//正在解锁中
    public static final int STATUS_UNLOCKED = 2;//已解锁
    private int mLockStatus = STATUS_LOCKED;
    private Context mContext;
    DialogLockBinding binding;

    public LockDialog(@NonNull Context context) {
        this(context, R.style.MyDialog, STATUS_LOCKED);
    }

    public LockDialog(@NonNull Context context, int lockStatus) {
        this(context, R.style.MyDialog, lockStatus);

    }

    private LockDialog(@NonNull Context context, int themeResId, int lockStatus) {
        super(context, themeResId);
        mContext = context;
        mLockStatus = lockStatus;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(mContext, R.layout.dialog_lock, null);
        binding = DataBindingUtil.bind(view);
        setContentView(view);
        binding.setCallback(mLockCallback);
        binding.editPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //输入当前时间打开彩蛋！
               String inputTxt= binding.editPwd.getText().toString();
               String curTime= TimeUtil.getCurrentTime("yyyyMMdd");
               if(TextUtils.equals(inputTxt,curTime)){
                   binding.editPwd.setText("");
                   openSystemLauncher();
               }
            }
        });
        switch (mLockStatus) {
            case STATUS_LOCKED:
                setCanceledOnTouchOutside(false);
                setCancelable(false);
                break;
            case STATUS_UNLOCKING:
                setCanceledOnTouchOutside(true);
                setCancelable(true);
                binding.rlayoutWarming.setVisibility(View.GONE);
                break;
            case STATUS_UNLOCKED:
                break;
        }
    }


    private void openSystemLauncher(){
        try {
            Intent intent = new Intent();
            intent.setClassName("com.android.launcher", "com.android.launcher2.Launcher");
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }catch (ActivityNotFoundException e){
            ToastUtils.showShortToast("打开默认Launcher界面失败");
        }
    }



    @Override
    public void onBackPressed() {
        switch (mLockStatus) {
            case STATUS_LOCKED:
                break;
            case STATUS_UNLOCKING:
                super.onBackPressed();
                break;
        }
    }

    private LockCallback mLockCallback = new LockCallback() {


        @Override
        public void onClick(int action) {
            switch (action) {
                case LockCallback.ACTION_UNLOCK:
                    binding.rlayoutWarming.setVisibility(View.GONE);
                    break;
                case LockCallback.ACTION_VERFICATION_UNLOCK:
                    //todo 验证密码
                    String pwd = binding.editPwd.getText().toString();
                    if (verficatePwd(pwd)) {
                        String encodePwd = EncryptUtils.encodeBase64String(EncryptUtils.encodeMD5String(pwd));
                        onUnlocked(encodePwd);
                    }
                    break;
            }
        }
    };

    public void hideLoading() {
        binding.layoutLoading.setVisibility(View.GONE);
    }

    public void showLoading() {
        binding.layoutLoading.setVisibility(View.VISIBLE);
    }


    private boolean verficatePwd(String pwd) {
        if (TextUtils.isEmpty(pwd)) {
            ToastUtils.showShortToast("解锁密码不能为空！");
            return false;
        }
        return true;
    }

    protected abstract void onUnlocked(String pwd);
}
