package com.yunqi.autovender.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.yunqi.autovender.R;
import com.yunqi.autovender.adapter.NumberAdapter;
import com.yunqi.autovender.databinding.PopwindowSelectBinding;
import com.yunqi.autovender.model.CargoInfo;
import com.yunqi.autovender.model.GlobalData;

import google.architecture.coremodel.callback.ConfirmCallback;
import google.architecture.coremodel.constant.ARGConstant;

public abstract class SelectPopwindow {
    public static final int TYPE_UNIT = 0;
    public static final int TYPE_ROW = 1;
    public static final int TYPE_COLUMN = 2;
    private PopupWindow popupWindow;
    private PopwindowSelectBinding mBinding;
    private Context mContext;
    private NumberAdapter mAdapter;
    private int mCurNumber;

    private int mType = TYPE_UNIT;//

    public SelectPopwindow(Context context, int type, int curNumber) {
        mContext = context;
        mType = type;
        mCurNumber = curNumber;
        View view = View.inflate(context, R.layout.popwindow_select, null);
        mBinding = DataBindingUtil.bind(view);
        mBinding.setCallback(mConfirmCallback);
        popupWindow = new PopupWindow(view, 500, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchable(true);
        initRecycleView();
    }


    private void initRecycleView() {
        mBinding.recyclerview.setLayoutManager(new GridLayoutManager(mContext, 3));
        int maxNumber = 0;
        CargoInfo cargoInfo = (CargoInfo) GlobalData.getInstance().getData(ARGConstant.ARG_CARGO_INFO);
        switch (mType) {
            case TYPE_ROW:
                maxNumber = cargoInfo.maxRow;
                break;
            case TYPE_COLUMN:
                maxNumber = cargoInfo.maxColumn;
                break;
            case TYPE_UNIT:
                maxNumber = cargoInfo.maxUnit;
                break;
        }
        int rowCount = (maxNumber + 1) % 3 == 0 ? (maxNumber + 1) / 3 : ((maxNumber + 1) / 3) + 1;
        if (rowCount > 4) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mBinding.recyclerview.getLayoutParams();
            lp.height = 420;
            mBinding.recyclerview.setLayoutParams(lp);
        }
        mAdapter = new NumberAdapter(mContext, maxNumber, mCurNumber);
        mBinding.recyclerview.setAdapter(mAdapter);
    }


    public void showAsDropDown(View anchor) {
        popupWindow.showAsDropDown(anchor, 0, 0);
    }

    private ConfirmCallback mConfirmCallback = new ConfirmCallback() {

        @Override
        public void onClick(int action) {
            if (action == ACTION_CONFIRM) {
                popupWindow.dismiss();
                onSelected(mAdapter.getCurNumber());
            } else if (action == ACTION_CANCEL) {
                if (mAdapter.getCurNumber() == 0) {
                    return;
                }
                mAdapter.resetLastNumber();
                mAdapter.setCurNumber(0);
            }
        }
    };

    protected abstract void onSelected(int index);

}
