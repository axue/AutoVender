package com.yunqi.autovender.view;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.DialogOfflineBinding;

import google.architecture.coremodel.callback.ConfirmCallback;
import google.architecture.coremodel.datamodel.http.entities.ProductData;

/**
 * 下架Dialog
 */
public abstract class OffLineDialog extends Dialog {
    private Context mContext;
    private ProductData.Product mProduct;
    public OffLineDialog(@NonNull Context context,@NonNull  ProductData.Product product) {
        this(context,R.style.MyDialog);
        mContext=context;
        mProduct=product;
    }



    private OffLineDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(mContext, R.layout.dialog_offline, null);
        DialogOfflineBinding binding = DataBindingUtil.bind(view);
        binding.setCallback(mConfirmCallback);
        setContentView(view);
        setCanceledOnTouchOutside(true);
    }

    private ConfirmCallback mConfirmCallback = new ConfirmCallback() {

        @Override
        public void onClick(int action) {
            dismiss();
            switch (action) {
                case ConfirmCallback.ACTION_CANCEL:
                    break;
                case ConfirmCallback.ACTION_CONFIRM:
                    onConfirmDelDrug(mProduct);
                    break;
            }
        }
    };


    public abstract void onConfirmDelDrug(ProductData.Product product);
}
