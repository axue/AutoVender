package com.yunqi.autovender.view;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.DialogPaymentBinding;
import com.yunqi.autovender.util.TextStyleUtil;


/**
 * 支付Dialog
 */
public class PaymentDialog extends Dialog {
    public Context mContext;
    private ImageView imgQRCode;
    private TextView txtPrice;
    private View viewLoading;
    private DialogPaymentBinding mBinding;
    private MyCountDownTimer mCountDownTimer;
    private double mPrice;
    public PaymentDialog(@NonNull Context context,double price) {
        this(context,R.style.MyDialog);
        mContext=context;
        mPrice=price;
    }

    @Override
    public void show() {
        super.show();
        showLoading();
    }


    /**
     * 开始倒计时
     */
    private void startCountDown(){
        mCountDownTimer= new MyCountDownTimer (60000, 1000);
        mCountDownTimer.start();
    }

    /**
     * 停止倒计时
     */
    public void stopCountDown(){
        if(mCountDownTimer!=null){
            mCountDownTimer.cancel();
        }
    }

    @Override
    public void dismiss() {
        stopCountDown();
        super.dismiss();
    }

    class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            int time=(int)millisUntilFinished / 1000;
            String formatStr=String.format(mContext.getString(R.string.txt_count_down),time);
            int start =formatStr.lastIndexOf("，");
            int end =formatStr.lastIndexOf("秒");
            TextStyleUtil.highStrWithIndex(mBinding.txtCountDown,formatStr,start+1,end);
        }

        @Override
        public void onFinish() {
            PaymentDialog.super.dismiss();
        }
    }

    private PaymentDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(mContext, R.layout.dialog_payment, null);
        mBinding = DataBindingUtil.bind(view);
        setContentView(view);
        setCanceledOnTouchOutside(true);
        imgQRCode=view.findViewById(R.id.img_qrcode);
        txtPrice=view.findViewById(R.id.txt_pay_price);
        viewLoading=view.findViewById(R.id.layout_loading);
        txtPrice.setText(String.valueOf(mPrice));
    }

    public void setQRCode(Bitmap bitmap){
        hideLoading();
        imgQRCode.setImageBitmap(bitmap);
        startCountDown();
    }


    public void hideLoading(){
        viewLoading.setVisibility(View.GONE);
    }

    private void showLoading(){
        viewLoading.setVisibility(View.VISIBLE);
    }
}
