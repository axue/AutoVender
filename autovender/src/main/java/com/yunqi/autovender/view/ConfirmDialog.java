package com.yunqi.autovender.view;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.DialogConfirmBinding;

import google.architecture.coremodel.callback.ConfirmCallback;

/**
 * 通用确认Dialog
 */
public class ConfirmDialog extends Dialog {
    private Context mContext;
    private ConfirmCallback mConfirmCallback;
    public ConfirmDialog(@NonNull Context context,ConfirmCallback confirmCallback) {
        this(context,R.style.MyDialog);
        mContext=context;
        mConfirmCallback=confirmCallback;
    }



    private ConfirmDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(mContext, R.layout.dialog_confirm, null);
        DialogConfirmBinding binding = DataBindingUtil.bind(view);
        binding.setCallback(mConfirmCallback);
        setContentView(view);
        setCanceledOnTouchOutside(true);
    }

}
