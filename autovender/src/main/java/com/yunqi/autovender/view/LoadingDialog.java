package com.yunqi.autovender.view;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.View;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.DialogLoadingBinding;


/**
 * 通用loading框
 */
public class LoadingDialog extends Dialog {
    private Context mContext;
    private String mLoadingStr;

    public LoadingDialog(@NonNull Context context,String loadingStr) {
        this(context, R.style.MyDialog);
        mLoadingStr=loadingStr;
        mContext = context;
    }

    public LoadingDialog(@NonNull Context context) {
        this(context, context.getString(R.string.tip_loading));
    }

    private LoadingDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(mContext, R.layout.dialog_loading, null);
        DialogLoadingBinding binding=DataBindingUtil.bind(view);
        setContentView(view);
        binding.txtLoading.setText(mLoadingStr);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK){

        }
        return super.onKeyDown(keyCode, event);
    }
}
