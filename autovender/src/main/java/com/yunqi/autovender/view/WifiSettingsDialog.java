package com.yunqi.autovender.view;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.yunqi.autovender.R;
import com.yunqi.autovender.databinding.DialogWifiSettingsBinding;

import google.architecture.coremodel.callback.ConfirmCallback;

/**
 * Wifi设置Dialog
 */
public class WifiSettingsDialog extends Dialog {
    private Context mContext;
    private ConfirmCallback mConfirmCallback;
    public WifiSettingsDialog(@NonNull Context context, ConfirmCallback confirmCallback) {
        this(context,R.style.MyDialog);
        mContext=context;
        mConfirmCallback=confirmCallback;
    }



    private WifiSettingsDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(mContext, R.layout.dialog_wifi_settings, null);
        DialogWifiSettingsBinding binding = DataBindingUtil.bind(view);
        binding.setCallback(mConfirmCallback);
        setContentView(view);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

}
