package com.yunqi.autovender.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.yunqi.autovender.R;

/**
 * 支付方式选择Dialog
 */
public class SelectPayTypeDialog extends Dialog implements View.OnClickListener {
    //支付宝支付方式
    public static final int PAY_TYPE_ALI=1;
    //微信支付方式
    public static final int PAY_TYPE_WX=2;

    private Context mContext;
    private PayTypeCallback mCallback;

    public SelectPayTypeDialog(@NonNull Context context, PayTypeCallback callback) {
        this(context, R.style.MyDialog);
        mContext = context;
        mCallback=callback;
    }

    private SelectPayTypeDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = View.inflate(mContext, R.layout.dialog_select_pay_type, null);
        setContentView(view);
        setCanceledOnTouchOutside(true);
        findViewById(R.id.img_alipay).setOnClickListener(this);
        findViewById(R.id.img_wxpay).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dismiss();
        switch (v.getId()) {
            case R.id.img_alipay:
                mCallback.onSelectPayType(PAY_TYPE_ALI);
                break;
            case R.id.img_wxpay:
                mCallback.onSelectPayType(PAY_TYPE_WX);
                break;
        }
    }

    public interface PayTypeCallback {
        void onSelectPayType(int payType);
    }
}
